import java.util.Random;
import java.util.Scanner;
 
// dice roller java source code
// Also outputs the dice face as ASCII art
public class DiceRollerInJava {

    //Jesse int for the RollCount
    public static int rollCount = 0;
   //Phuc and Jesse declared int and method for odd and even number 
	public static int even = 0;
    public static int odd = 0;
    static void freq(int value) 
	{ 
    if ( value % 2 == 0 )
        even++;
    else
        odd++;
  }

    // This has printing information for all numbers
    // For each number,3x3 matrix represents the face
    int[][][] faceConfig = { { { 0, 0, 0 }, { 0, 1, 0 }, { 0, 0, 0 } }, 
                           { { 0, 0, 1 }, { 0, 0, 0 }, { 1, 0, 0 } },
                           { { 0, 0, 1 }, { 0, 1, 0 }, { 1, 0, 0 } }, 
                           { { 1, 0, 1 }, { 0, 0, 0 }, { 1, 0, 1 } },
                           { { 1, 0, 1 }, { 0, 1, 0 }, { 1, 0, 1 } }, 
                           { { 1, 0, 1 }, { 1, 0, 1 }, { 1, 0, 1 } } };
 
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        DiceRollerInJava dice = new DiceRollerInJava();
        int temp=0;//Declare varible-Jack
        while (true) {

            System.out.println("How many Dice would you like to roll?");
            int diceAmountValue = scanner.nextInt();
            
            //Jesse Added code for selecting how many dice to roll
            for(int i = 0; i < diceAmountValue; i++){
            int result = dice.roll();
            freq(result);
            temp=temp+result;//Temporarily store the roll-Jack
			
            //Jesse Changed to add rollcount output + string to make more sense
            System.out.println("Your " + rollCount + " dice face value is :" + result);
            dice.draw(result);
            System.out.println("The total value rolled until now is: " + temp); //display total value-Jack
            //Phuc added result output whether it is odd or even number
			if (result% 2 == 0 )
			System.out.print("Odd number = " + result + " \n"); 
			else
			System.out.print("Even number = " + result + " \n"); 
 
            }
            //Aaron added clarity to the Roll Again Function
            System.out.println("Type 'no' to quit. Or Continue with the next step to roll again:");
            String input = scanner.nextLine();
            if (input.equalsIgnoreCase("n") || 
                    input.equalsIgnoreCase("no")) {
                System.out.println("Bye!");
                scanner.close();
                return;
            }
        }
    }
 
    // Draw the dice face using ascii characters
    private void draw(int value) {
        int[][] displayVal = faceConfig[value - 1];
        System.out.println("-----");
 
        for (int i = 0; i < 3; i++) {
            System.out.print("|");
            for (int j = 0; j < 3; j++) {
                if (displayVal[i][j] == 1) {
                    System.out.print("o");
                } else {
                    System.out.print(" ");
                }
            }
            System.out.println("|");
        }
        System.out.println("-----");
 
    }
 
    // Roll the dice in Java
    private int roll() {

        Random r = new Random();

        //Jesse - Increasing RollCount
        rollCount++;

        return r.nextInt(6) + 1;
    }

}