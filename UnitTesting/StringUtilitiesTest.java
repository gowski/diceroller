public class StringUtilitiesTest
{
   public static void combineArrayStringWithDelimiterTest()
   {
      //Test case
      StringUtilities s0 = new StringUtilities();
      String[] stringArray = { "DP1",  "is",  "cool" };
      String delimiter = " " ;
      System.out.println("The test case is:");
      System.out.println("The strings to be combined:");
      for (String item : stringArray)
      {
         System.out.println("\'" + item + "\'");
      }
      System.out.println("The delimiter is:");
      System.out.println("\'" + delimiter + "\'");
      //Expected output
      String expected = "DP1 is cool";
      System.out.println("The expected output is: \'" + expected + "\'");
      //Actual output
      String actual = s0.combineArrayStringWithDelimiter(stringArray, delimiter);
      System.out.println("The actual output is: \'" + actual + "\'");
      //Decide the testing result
      if(actual.equals(expected)) System.out.println("Pass!");
      else System.out.println("Fail!");
   }
   public static void replaceSpacesInStringByHyphensTest()
   { 
      //Test case
      StringUtilities s0 = new StringUtilities();
      String aString = "DP1 is cool";
      System.out.println("The test case is:");
      System.out.println("The strings to be modified is: ");
      System.out.println("\'" + aString + "\'");
      //Expected output
      String expected = "DP1-is-cool";
      System.out.println("The expected output is: \'" + expected + "\'");
      //Actual output
      String actual = s0.replaceSpacesInStringByHyphens(aString);
      System.out.println("The actual output is: \'" + actual + "\'");
      //Decide the testing result
      if(actual.equals(expected)) System.out.println("Pass!");
      else System.out.println("Fail!");
   }
   public static void main(String[] args) 
   {
      System.out.println("Unit Testing");
      System.out.println("====================================================");
      System.out.println("Test the method \"combineArrayStringWithDelimiter\":");
      combineArrayStringWithDelimiterTest();
      System.out.println("====================================================");
      System.out.println("Test the method \"replaceSpacesInStringByHyphens\":");
      replaceSpacesInStringByHyphensTest();
   }
}