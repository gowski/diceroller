public class StringUtilities
{
   public String combineArrayStringWithDelimiter(String[] stringArray, String delimiter)
   {
      String str = "";
      for (String item : stringArray)
      {
         str += item + delimiter;
      }
      return str;
   }
   public String replaceSpacesInStringByHyphens(String aString)
   {
      String[] stringArray = aString.split(" ");
      return combineArrayStringWithDelimiter(stringArray, "-");
   }
}