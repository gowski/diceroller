import java.util.Random;
import java.util.Scanner;
 
// dice roller java source code
// Also outputs the dice face as ASCII art
public class DiceRollerInJava {

    //Jesse int for the RollCount
    public static int rollCount = 0;
    public static int even = 0;
    public static int odd = 0;
    // This has printing information for all numbers
    // For each number,3x3 matrix represents the face
    static void freq(int value) 
	{ 
        if ( value % 2 == 0 )
        even++;
     else
        odd++;
  }
	
 int faceConfig[][][] = { { { 0, 0, 0 }, { 0, 1, 0 }, { 0, 0, 0 } }, 
                           { { 0, 0, 1 }, { 0, 0, 0 }, { 1, 0, 0 } },
                           { { 0, 0, 1 }, { 0, 1, 0 }, { 1, 0, 0 } }, 
                           { { 1, 0, 1 }, { 0, 0, 0 }, { 1, 0, 1 } },
                           { { 1, 0, 1 }, { 0, 1, 0 }, { 1, 0, 1 } }, 
                           { { 1, 0, 1 }, { 1, 0, 1 }, { 1, 0, 1 } } };
    public static void main(String[] args) 
	{
		
		Scanner scanner = new Scanner(System.in);
        DiceRollerInJava dice = new DiceRollerInJava();
        while (true) {
            int result = dice.roll();
            freq(result);
            //Jesse Changed to add rollcount output + string to make more sense
            System.out.println("Your " + rollCount + " dice face value is :" + result);
			System.out.print("Odd number = " + 
                       odd + " \n"); 
			System.out.print("Even number = " + 
                       even + " \n"); 
			
            System.out.println("Roll again? (type no to quit):");
            String input = scanner.nextLine();
            if (input.equalsIgnoreCase("n") || 
                    input.equalsIgnoreCase("no")) {
                System.out.println("Bye!");
                scanner.close();
                return;
            }
        }
    }
 
    // Draw the dice face using ascii characters
    private void draw(int value) {
        int[][] displayVal = faceConfig[value - 1];
        System.out.println("-----");
 
        for (int i = 0; i < 3; i++) {
            System.out.print("|");
            for (int j = 0; j < 3; j++) {
                if (displayVal[i][j] == 1) {
                    System.out.print("o");
                } else {
                    System.out.print(" ");
                }
            }
            System.out.println("|");
        }
        System.out.println("-----");
 
    }
 
    // Roll the dice in Java
    private int roll() {
        Random r = new Random();

        //Jesse - Increasing RollCount
        rollCount++;

        return r.nextInt(6) + 1;
    }
}